import React from "react";
import { Router } from "@reach/router";
import { useAuth } from "@renemn/gatsby-plugin-client-only-auth";

import GetStarted from "../components/GetStarted";
import Home from "../components/Home";
import NotFound from "../components/NotFound";
import SignIn from "../components/SignIn";
import SignUp from "../components/SignUp";
import Splash from "../components/Splash";
import User from "../components/User";

import {
  PRIVATE_DEFAULT_ROUTE,
  PUBLIC_DEFAULT_ROUTE,
  SIGN_IN_ROUTE,
  SIGN_UP_ROUTE,
  USER_ROUTE,
} from "../constants/routes";

function App() {
  const [authState] = useAuth();
  return authState.isLoading ? (
    <Splash />
  ) : (
    <Router>
      <NotFound default />
      <GetStarted path={PUBLIC_DEFAULT_ROUTE} />
      <SignIn path={SIGN_IN_ROUTE} />
      <SignUp path={SIGN_UP_ROUTE} />
      <Home path={PRIVATE_DEFAULT_ROUTE} />
      <User path={USER_ROUTE} />
    </Router>
  );
}

export default App;
